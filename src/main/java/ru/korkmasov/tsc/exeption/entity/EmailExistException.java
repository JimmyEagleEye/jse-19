package ru.korkmasov.tsc.exeption.entity;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException(String value) {
        super("Error. Email '" + value + "' already exist.");
    }

}
