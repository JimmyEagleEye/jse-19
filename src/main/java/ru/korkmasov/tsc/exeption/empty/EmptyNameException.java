package ru.korkmasov.tsc.exeption.empty;


import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
