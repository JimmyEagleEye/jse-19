package ru.korkmasov.tsc.exeption.empty;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty...");
    }

}
