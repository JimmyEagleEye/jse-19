package ru.korkmasov.tsc.exeption.system;

import ru.korkmasov.tsc.exeption.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}
