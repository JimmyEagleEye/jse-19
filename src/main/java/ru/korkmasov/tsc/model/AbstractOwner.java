package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.api.entity.ITWBS;

public abstract class AbstractOwner extends AbstractEntity implements ITWBS {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

