package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exeption.entity.TaskNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.Arrays;

public class TaskByNameSetStatusCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-name";
    }

    @Override
    public String description() {
        return "Set task status by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsByName(userId, name)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().changeTaskStatusByName(userId, name, Status.getStatus(TerminalUtil.nextLine()));
    }

}
