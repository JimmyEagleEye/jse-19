package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.exeption.empty.EmptyNameException;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.util.ValidationUtil;

public final class TaskByNameUpdateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-name";
    }

    @Override
    public String description() {
        return "Update task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsByName(userId, name)) throw new EmptyNameException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (ValidationUtil.isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getTaskService().updateTaskByName(userId, name, nameNew, TerminalUtil.nextLine());
    }

}
