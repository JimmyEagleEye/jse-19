package ru.korkmasov.tsc.command.user;

import ru.korkmasov.tsc.exeption.empty.EmptyEmailException;
import ru.korkmasov.tsc.exeption.entity.LoginExistException;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.util.ValidationUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-registry";
    }

    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().existsByLogin(login)) throw new LoginExistException(login);
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        if (ValidationUtil.isEmpty(email)) throw new EmptyEmailException();
        System.out.println("ENTER PASSWORD:");
        serviceLocator.getAuthService().registry(login, TerminalUtil.nextLine(), email);
    }

}