package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findALLTaskByProjectId(final String userId, final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findALLTaskByProjectId(userId, projectId);
        list.removeAll(listByProject);
        return listByProject;
    }

    @Override
    public Task assignTaskByProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unassignTaskByProjectId(final String userId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(final String userId, Task task) {
        List<Task> list = findAll(userId);
        list.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        List<Task> list = findAll(userId);
        list.remove(task);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        List<Task> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        for (int i = list.size(); i-- > 0; ) {
            if (projectId.equals(list.get(i).getProjectId()) && userId.equals(list.get(i).getUserId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }

    @Override
    public String getIdByIndex(int index) {
        return list.get(index).getId();
    }

}








