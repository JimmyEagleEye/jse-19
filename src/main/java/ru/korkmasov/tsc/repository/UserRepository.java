package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IUserRepository;
import ru.korkmasov.tsc.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    /*@Override
    public void clear(String userId) {

    }

    @Override
    public List<User> findAll(String userId, Comparator<User> comparator) {
        return null;
    }

    @Override
    public List<User> findAll(String userId) {
        return null;
    }*/

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean existsByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    public boolean existsByEmail(final String email) {
        for (final User user : list) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }
}
