package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.ValidationUtil;


import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (ValidationUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String arg = command.arg();
            if (ValidationUtil.isEmpty(arg)) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String name) {
        return arguments.get(name);
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
