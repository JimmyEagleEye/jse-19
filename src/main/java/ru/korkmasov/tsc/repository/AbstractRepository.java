package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public int size() {
        return list.size();
    }

    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return null;
    }

    public E findById(final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }
}



