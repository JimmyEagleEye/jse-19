package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.model.Project;

public interface IProjectService extends IOwnerService<Project> {

    Project findOneById(String userId, String id);

    Project add(String userId, String name, String description);

    Project removeOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    Project removeOneByName(String userId, String name);

    Project removeProjectByIndex(String userId, Integer index);

    Project findOneByIndex(String userId, Integer index);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByName(String userId, String name, String nameNew, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByIndex(String userId, Integer index);

    Project startProjectByName(String userId, String name);

    Project finishProjectById(String userId, String id);

    Project finishProjectByIndex(String userId, Integer index);

    Project finishProjectByName(String userId, String name);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    boolean existsByName(String userId, String name);

}
