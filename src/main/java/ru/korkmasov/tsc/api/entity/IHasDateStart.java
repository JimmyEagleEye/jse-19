package ru.korkmasov.tsc.api.entity;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date dateStart);

}
